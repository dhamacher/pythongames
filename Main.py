__author__ = 'dhamache'

"""
Loading modules commonly used in pygames
"""
try:
    import sys
    import random
    import math
    import os
    import getopt
    import pygame
    from socket import *
    from pygame.locals import *
except ImportError, err:
    print "Unable to load module. %s" % (err)
    sys.exit(2)


def load_png(name):
    """
    :param name: Name of the image to load
    :return: image object
    """
    fullname = os.path.join('images', name)
    try:
        image = pygame.image.load(fullname)
        if image.get_alpha() is None:
            image = image.convert()
        else:
            image = image.convert_alpha()
    except pygame.error, message:
        print "Cannot load image: %s" % (fullname)
        raise SystemExit, message
    return image, image.get_rect()


class Ball(pygame.sprite.Sprite):
    """ A ball object that will move across the screen
    Returns: ball object
    Functions: calculate_position, update
    Attributes: are, vector"""

    def __init__(self, vector):
        pygame.sprite.Sprite.__init__(self)
        self.image, self.rect = load_png('ball.png')
        screen = pygame.display.get_surface()
        self.area = screen.get_rect()
        self.vector = vector

    def update(self):
        new_position = calculate_position(self.rect, self.vector)
        self.rect = new_position

    def calculate_position(self, rect, vector):
        (angle, z) = vector
        (dx, dy) = (z * math.cos(angle), z * math.sin(angle))
        return rect.move(dx, dy)

class Bat(pygame.sprite.Sprite):
    """ Moveable bat object to hit the ball with
    Returns: bat object
    Functions: reinit, update, moveup, movedown
    Attributes: which, speed"""

    def __init__(self):
        pygame.sprite.Sprite.__init__(self)
        self.image, self, rect = load_png('bat.png')




# Color definitions
BLACK = (0, 0, 0)
WHITE = (255, 255, 255)
GREEN = (0, 255, 0)
RED = (255, 0, 0)


class MainWindow:

    def __init__(self):
        self.engine = pygame.init()
        self.screen_size = (800, 600)
        self.game_screen = pygame.display.set_mode(self.screen_size)
        pygame.display.set_caption("Games in Python")
        self.game_exit = False
        self.clock = pygame.time.Clock()

    def game_menu(self):
        pass

    def start_game(self):
        pass

    def open_screen(self):
        pass

    def game_loop(self):
        while not self.game_exit:
            # Main event loop
            for event in pygame.event.get():
                if event.type == pygame.Quit:
                    self.game_exit = True

            # Game logig goes here (collisions etc)...

            # Clear the screen
            self.game_screen.fill(WHITE)

            # Drawing code goes here....

            # Update the screen
            pygame.display.flip()

            # Limit frames per second
            self.clock.tick(60)

if __name__ == '__main__':
    main = MainWindow()
    main.game_loop()